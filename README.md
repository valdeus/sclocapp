# SCLocalizationUA
[![Crowdin](https://badges.crowdin.net/star-citizen-localization-ua/localized.svg)](https://shorturl.at/dopMW)
[![Website](https://img.shields.io/website?url=https%3A%2F%2Fusf.42web.io%2F&down_message=SITE&style=flat&label=USF&labelColor=blue&color=yellow)](https://usf.42web.io/)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/valdeus/sclocapp/-/blob/main/LICENSE)
[![Virustotal](https://img.shields.io/static/v1?label=Virustotal&message=Scan&color=blue)](https://www.virustotal.com/gui/file/709552463786858d54e9285df1e69d8f4acd9130a614b42f4490344b8f7fb70a?nocache=1)


Додаток для локалізації гри Star Citizen Українською

Підтримка Української локалізації гри зроблена учасниками [UKRAINIAN SPACE FLEET](https://robertsspaceindustries.com/orgs/UKR)

[![Website](https://img.shields.io/website?url=https%3A%2F%2Fsend.monobank.ua%2Fjar%2F44HXkQkorg&up_message=%D0%9F%D1%80%D0%BE%D0%B5%D0%BA%D1%82&style=for-the-badge&label=%D0%9F%D1%96%D0%B4%D1%82%D1%80%D0%B8%D0%BC%D0%B0%D1%82%D0%B8)
](https://send.monobank.ua/jar/44HXkQkorg)


[Посилання на переклад](https://gitlab.com/valdeus/sc_localization_ua/-/releases/permalink/latest)

Повідомити про помилку в перекладі або приєднатись до проєкту можна через наш [Discord](https://discord.gg/TkaN6Yv4VT)

### Важливо:
Ця програма не втручається жодним чином в файли або пам'ять гри. Ми дотримуємося угоди про використання гри і не намагаємося отримати незаслужені переваги чи порушити її баланс.

Основна функціональність:
Ми пропонуємо можливість додати локалізацію до гри Star Citizen, включаючи переклад вмісту гри. Ці зміни не впливають на геймплей чи ігровий баланс, а лише допомагають гравцям насолоджуватися грою на своїй рідній мові.

## Інструкція

1. Завантажте останню версію з [релізів](https://gitlab.com/valdeus/sc_localization_ua/-/releases/permalink/latest)
2. Запустіть інсталятор та встановіть додаток у зручну для вас папку.
3. Запустіть додаток через ярлик на робочому столі або з папки встановлення та виберіть папку з грою `[StarCitizen\LIVE]`
(при наступному запуску додатка шлях буде вибраний автоматично
4. Включіть прапорець `user.cfg` якщо у вас не має цього файлу (конфігурації користувача) або створіть його власноруч з [інструкції](https://gitlab.com/valdeus/sc_localization_ua/-/blob/main/README.md)
5. Натисніть кнопку "встановити локалізацію"
   
